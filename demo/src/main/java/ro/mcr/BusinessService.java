/**
 * 
 */
package ro.mcr;

import java.util.List;
import java.util.Optional;

import ro.mcr.cache.CacheServiceFactory;
import ro.mcr.data.ContactProfile;
import ro.mcr.data.Notification;
import ro.mcr.notification.CompositeNotificationService;

/**
 * @author mcraciunescu
 *
 */
public class BusinessService {
	
	private ICacheService cacheService = CacheServiceFactory.getActiveCache();
	private INotificationService notificationService = new CompositeNotificationService();
	
	public void register(ContactProfile contactProfile){
		
		System.out.println("Registering... " + contactProfile.getName());
	
		validateContactRegisterInCache(contactProfile);
		cacheService.add(contactProfile);
		notificationService.notifyRegister(contactProfile);
		
		System.out.println("Registed: " + contactProfile);
	}
	
	public Optional<ContactProfile> getByMobile(Long mobile){
		return cacheService.getByMobile(mobile);
	}

	private void validateContactRegisterInCache(ContactProfile contactProfile) {
		if ( cacheService.contains(contactProfile.getMobile())){
			throw new IllegalArgumentException("Already registered");
		}
	}

	public List<ContactProfile> getProfiles(int size) {		
		return cacheService.get(size);
	}

	public List<Notification> getNotifications(int size) {
		return cacheService.getNotifications(size);
	}
	
	

}
