/**
 * 
 */
package ro.mcr;

import ro.mcr.data.ContactProfile;

/**
 * @author mcraciunescu
 *
 */
public interface INotificationService {

	void notifyRegister(ContactProfile contactProfile);
	
}
