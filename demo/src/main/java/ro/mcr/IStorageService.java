/**
 * 
 */
package ro.mcr;

import java.util.List;
import java.util.Optional;

import ro.mcr.data.ContactProfile;
import ro.mcr.data.Notification;

/**
 * @author mcraciunescu
 *
 */
public interface IStorageService{

	void add(ContactProfile contactProfile);

	boolean contains(Long mobile);

	Optional<ContactProfile> getByMobile(Long mobile);
	
	List<ContactProfile> get(int size);
	
	List<Long> getNotifiableContacts(Long contactMobile);
	
	void addNotification(Notification n);
	
	List<Notification> getNotifications(int size);

}
