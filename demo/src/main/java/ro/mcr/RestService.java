/**
 * 
 */
package ro.mcr;

import org.restlet.Application;
import org.restlet.Component;
import org.restlet.Restlet;
import org.restlet.data.Protocol;
import org.restlet.resource.Directory;
import org.restlet.routing.Router;
import org.restlet.routing.Template;

import ro.mcr.config.ServerConfig;
import ro.mcr.rest.ContactResource;
import ro.mcr.rest.ContactsResource;
import ro.mcr.rest.NotificationsResource;

/**
 * @author mcraciunescu
 *
 */
public class RestService extends Application {

	private static final ServerConfig SERVER_CONFIG = new ServerConfig();
	public static final String ROOT_URI_PREFIX = "file://";

	public static void main(String[] args) throws Exception {

		// Create a new Component.
		Component component = new Component();

		// Add a new HTTP server listening on port 8182.
		component.getServers().add(Protocol.HTTP, SERVER_CONFIG.getServerHttpPort());
		component.getClients().add(Protocol.FILE);

		// Attach the sample application.
		component.getDefaultHost().attach("/app", new RestService());

		// Start the component.
		component.start();
	}

	/**
	     * Creates a root Restlet that will receive all incoming calls.
	     */
	    @Override
	    public synchronized Restlet createInboundRoot() {
	        Router router = new Router(getContext());

	        router.attach("/rest/contacts/{size}", ContactsResource.class);
	        router.attach("/rest/contacts/contact/register", ContactResource.class);
	        router.attach("/rest/contacts/contact/{mobile}", ContactResource.class);
	        
	        router.attach("/rest/notifications/{size}", NotificationsResource.class);
	        
	        // Create a directory able to expose a hierarchy of files
	        Directory directory = new Directory(getContext(), ROOT_URI_PREFIX + SERVER_CONFIG.getServerStaticFilesRootPath());
	        router.attach("/html", directory).setMatchingMode(Template.MODE_STARTS_WITH);

	        

	        return router;
	    }

}
