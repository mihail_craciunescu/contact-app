/**
 * 
 */
package ro.mcr.cache;

import ro.mcr.ICacheService;

/**
 * @author mcraciunescu
 *
 */
public class CacheServiceFactory {
	
	public static ICacheService getActiveCache(){
		return new NoCacheService();
	}

}
