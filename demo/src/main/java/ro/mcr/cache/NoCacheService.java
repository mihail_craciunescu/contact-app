/**
 * 
 */
package ro.mcr.cache;

import ro.mcr.ICacheService;
import ro.mcr.storage.DynamoDBStorage;

/**
 * @author mcraciunescu
 *
 */
class NoCacheService extends DynamoDBStorage implements ICacheService {

}
