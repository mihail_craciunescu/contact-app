/**
 * 
 */
package ro.mcr.config;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import myjava.utils.properties.CommonConfigProperties.IFileResolver;

/**
 * @author mcraciunescu
 *
 */
class ConfigFileResolver implements IFileResolver {

	private static final String CONFIG_FILENAME = "config.properties";
	
	public InputStream getFileAsInputStream() {
		InputStream configFile = null;
		try {
			configFile = new FileInputStream(CONFIG_FILENAME);
		} catch (FileNotFoundException e) {
			throw new IllegalArgumentException("config.properties should be in app running folder");
		}
		return configFile;
	}

}
