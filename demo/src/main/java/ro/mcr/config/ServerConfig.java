/**
 * 
 */
package ro.mcr.config;

import myjava.utils.properties.CommonConfigProperties;

/**
 * @author mcraciunescu
 *
 */
public class ServerConfig extends CommonConfigProperties {

	private static final long serialVersionUID = 1L;

	public ServerConfig() {
		super(new ConfigFileResolver());
	}

	public int getServerHttpPort() {
		String portStr = getProperty("server.port.http", "8182");
		int port = Integer.parseInt(portStr);
		return port;
	}
	
	public String getServerStaticFilesRootPath() {
		String rootPath = getProperty("server.directory.rootpath", "/tmp");
		return rootPath;
	}

}
