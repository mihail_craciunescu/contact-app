/**
 * 
 */
package ro.mcr.data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author mcraciunescu
 *
 */
public class ContactProfile implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name;
	private Long mobile;
	private Map<Long, String> contacts = new HashMap<>();
	
	private String registrationId;
	
	public ContactProfile() {
	}
	
	public ContactProfile(String name, Long mobile) {
		super();
		this.name = name;
		this.mobile = mobile;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getMobile() {
		return mobile;
	}
	public void setMobile(Long mobile) {
		this.mobile = mobile;
	}

	public Map<Long, String> getContacts() {
		return contacts;
	}

	public void setContacts(Map<Long, String> contacts) {
		this.contacts = contacts;
	}

	public String getRegistrationId() {
		return registrationId;
	}

	public void setRegistrationId(String registrationId) {
		this.registrationId = registrationId;
	}

	@Override
	public String toString() {
		return "ContactProfile [name=" + name + ", mobile=" + mobile + ", contacts=" + contacts + ", registrationId="
				+ registrationId + "]";
	}

}
