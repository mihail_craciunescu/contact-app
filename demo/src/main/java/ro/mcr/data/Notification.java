/**
 * 
 */
package ro.mcr.data;

import java.time.LocalDateTime;

/**
 * @author mcraciunescu
 *
 */
public class Notification {
	
	private Long mobile;
	private String content;
	private String type;
	private LocalDateTime timestamp;
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public LocalDateTime getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}
	public Long getMobile() {
		return mobile;
	}
	public void setMobile(Long mobile) {
		this.mobile = mobile;
	}
	@Override
	public String toString() {
		return "Notification [mobile=" + mobile  + ", type=" + type + ", timestamp=" + timestamp+ ", content=" + content
				+ "]";
	}


}
