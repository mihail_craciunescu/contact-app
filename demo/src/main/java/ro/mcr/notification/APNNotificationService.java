package ro.mcr.notification;

import java.time.LocalDateTime;
import java.util.List;

import ro.mcr.data.ContactProfile;
import ro.mcr.data.Notification;

/**
 * @author mcraciunescu
 *
 */
public class APNNotificationService extends AbstractNotificationService {

	@Override
	public void notifyRegister(ContactProfile contactProfile) {
		List<Long> notifiableContacts = cacheService.getNotifiableContacts(contactProfile.getMobile());
		
		// TODO notify appropriate contacts
		notifiableContacts.forEach(n -> sendNotification(contactProfile, n));		
		
	}
	
	private void sendNotification(ContactProfile contactProfile, Long mobile){
		if (mobile.toString().startsWith("123")){
			System.out.println("Send Apple [NotifyRegister] of: " + contactProfile.getMobile() + " to: "+mobile);
			Notification n = new Notification();
			n.setMobile(mobile);
			n.setTimestamp(LocalDateTime.now());
			n.setType("APPLE");
			n.setContent("user registered: "+contactProfile);
			cacheService.addNotification(n);
		}
	}

}
