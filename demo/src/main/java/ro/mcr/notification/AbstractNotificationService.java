/**
 * 
 */
package ro.mcr.notification;

import ro.mcr.ICacheService;
import ro.mcr.INotificationService;
import ro.mcr.cache.CacheServiceFactory;

/**
 * @author mcraciunescu
 *
 */
abstract class AbstractNotificationService implements INotificationService {

	protected ICacheService cacheService = CacheServiceFactory.getActiveCache();

}
