/**
 * 
 */
package ro.mcr.notification;

import java.util.ArrayList;
import java.util.List;

import ro.mcr.INotificationService;
import ro.mcr.data.ContactProfile;

/**
 * @author mcraciunescu
 *
 */
public class CompositeNotificationService implements INotificationService {

	
	private List<INotificationService> services = new ArrayList<>();
	{
		services.add(new APNNotificationService());
		services.add(new GCMNotificationService());
	}
	
	@Override
	public void notifyRegister(ContactProfile contactProfile) {
		services.forEach(s ->s.notifyRegister(contactProfile));
	}

}
