/**
 * 
 */
package ro.mcr.rest;

import org.restlet.resource.ServerResource;

import ro.mcr.BusinessService;

/**
 * @author mcraciunescu
 *
 */
public class AbstractResource extends ServerResource {
	protected BusinessService businessService = new BusinessService();
}
