/**
 * 
 */
package ro.mcr.rest;

import java.util.Optional;

import org.restlet.data.Form;
import org.restlet.data.Status;
import org.restlet.resource.Get;
import org.restlet.resource.Post;

import ro.mcr.data.ContactProfile;

/**
 * @author mcraciunescu
 *
 */
public class ContactResource extends AbstractResource implements IContactResource{
	
	@Get("json")
	public ContactProfile getContact(){
		 //String mobile =  getQuery().getValues("mobile");
		 String mobile = (String) getRequest().getAttributes().get("mobile");
    	 if (mobile == null){
    		 getResponse().setStatus(Status.CLIENT_ERROR_NOT_FOUND);
    		 return null;
    	 }
        Optional<ContactProfile> contact = businessService.getByMobile(Long.parseLong(mobile.toString()));
        if (!contact.isPresent()){
        	getResponse().setStatus(Status.CLIENT_ERROR_NOT_FOUND);
        	return null;
        }
        ContactProfile c = contact.get();
        //JacksonRepresentation<ContactProfile> result = 	    new JacksonRepresentation<ContactProfile>(c);
        return c;
	}
	

	 @Post
    public void register(Form entity) {
    	getResponse().setAccessControlAllowOrigin("*");
        ContactProfile contactProfile = new ContactProfile();
        
        contactProfile.setMobile(Long.parseLong(entity.getValues("mobile")));
        contactProfile.setName(entity.getValues("name"));
        String contacts = entity.getValues("contacts");
        String[] cList =contacts.split(";");
        for (int i = 0; i < cList.length; i++) {
        	String c = cList[i];
			if (!c.trim().isEmpty()){
				Long mobile = Long.parseLong(c.trim().split(",")[0]);
				String text = c.trim().split(",")[1];
				contactProfile.getContacts().put(mobile, text);
			}
		}
        
		businessService.register(contactProfile );
    }
	 
}
