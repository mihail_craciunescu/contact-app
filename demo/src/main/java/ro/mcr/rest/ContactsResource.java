/**
 * 
 */
package ro.mcr.rest;

import java.util.List;

import ro.mcr.data.ContactProfile;

/**
 * @author mcraciunescu
 *
 */
public class ContactsResource extends AbstractResource implements IContactsResource{

    public  List<ContactProfile> getContacts() {
//    	 String sizeStr =  getQuery().getValues("size");
    	 String sizeStr = (String) getRequest().getAttributes().get("size");
    	 int size;
    	 if (sizeStr == null){
    		 size=10;
    	 }else{
    		 size = Integer.parseInt(sizeStr);
    	 }
        List<ContactProfile> contacts = businessService.getProfiles(size);
        //JacksonRepresentation< List<ContactProfile>> result = 	    new JacksonRepresentation<>(contacts);
        return contacts;
    }
    
}
