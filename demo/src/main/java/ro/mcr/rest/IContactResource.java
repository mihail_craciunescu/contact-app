/**
 * 
 */
package ro.mcr.rest;

import org.restlet.data.Form;
import org.restlet.resource.Get;
import org.restlet.resource.Post;

import ro.mcr.data.ContactProfile;

/**
 * @author mcraciunescu
 *
 */
public interface IContactResource {
	@Get("json")
	ContactProfile getContact();
	
	@Post
    void register(Form entity);
}
