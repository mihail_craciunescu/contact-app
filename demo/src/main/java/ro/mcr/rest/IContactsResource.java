/**
 * 
 */
package ro.mcr.rest;

import java.util.List;

import org.restlet.resource.Get;

import ro.mcr.data.ContactProfile;

/**
 * @author mcraciunescu
 *
 */
public interface IContactsResource {

	@Get("json")
     List<ContactProfile> getContacts();
}
