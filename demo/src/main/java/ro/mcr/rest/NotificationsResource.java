/**
 * 
 */
package ro.mcr.rest;

import java.util.List;

import org.restlet.resource.Get;

import ro.mcr.data.Notification;

/**
 * @author mcraciunescu
 *
 */
public class NotificationsResource extends AbstractResource{

	@Get("json")
    public  List<Notification> getNotifications() {
		getResponse().setAccessControlAllowOrigin("*");
    	 String sizeStr = (String) getRequest().getAttributes().get("size");
    	 int size;
    	 if (sizeStr == null){
    		 size=10;
    	 }else{
    		 size = Integer.parseInt(sizeStr);
    	 }
        List<Notification> contacts = businessService.getNotifications(size);
        return contacts;
    }
    
}
