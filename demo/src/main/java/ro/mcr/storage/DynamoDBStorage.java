/**
 * 
 */
package ro.mcr.storage;

import java.time.ZoneOffset;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

import ro.mcr.IStorageService;
import ro.mcr.data.ContactProfile;
import ro.mcr.data.Notification;

/**
 * @author mcraciunescu
 *
 */
public class DynamoDBStorage implements IStorageService {
	
	private static ConcurrentMap<Long, ContactProfile> profiles = new ConcurrentHashMap<>();
	private static ConcurrentMap<Long, List<Long>> profileToNotifiableContactsMap = new ConcurrentHashMap<>();
	
	private static ConcurrentMap<String, Notification> notifications = new ConcurrentHashMap<>();
	
	static{
		ContactProfile test = new ContactProfile("test",0L);
		profiles.put(0L, test );
	}
	@Override
	public void add(ContactProfile contactProfile) {
		ContactProfile result = profiles.putIfAbsent(contactProfile.getMobile(), contactProfile);
		if (result !=null){
			throw new IllegalArgumentException("Already registered");
		}
		
		//compute contacts to be notified
		contactProfile.getContacts().forEach((n,s)->{
			profileToNotifiableContactsMap.compute(n, (nr,l) ->{
				List<Long> newList= l;
				if(l==null){
					newList= new LinkedList<>();
				}
				newList.add(contactProfile.getMobile());
				
				return newList;
			});
		});
	}

	@Override
	public boolean contains(Long mobile) {
		return profiles.containsKey(mobile);
	}

	@Override
	public Optional<ContactProfile> getByMobile(Long mobile) {
		ContactProfile contact = profiles.get(mobile);
		return Optional.ofNullable(contact);
	}

	@Override
	public List<ContactProfile> get(int size) {
		return profiles.values().stream().limit(size).collect(Collectors.toList());
	}

	@Override
	public List<Long> getNotifiableContacts(Long contactMobile) {
		return profileToNotifiableContactsMap.getOrDefault(contactMobile, new LinkedList<>());
	}
	
	@Override
	public void addNotification(Notification n){
		String key = "" + n.getTimestamp().toInstant(ZoneOffset.UTC) +"-"+ n.getMobile();		
		notifications.put(key , n);
	}

	@Override
	public List<Notification> getNotifications(int size) {
		List<String> keyList = new LinkedList<>(notifications.keySet());
		Collections.sort(keyList);
		List<Notification> nList = keyList.parallelStream().limit(size).map(k -> notifications.get(k)).collect(Collectors.toList());
		Collections.reverse(nList);
		return nList;
	}
	
	

}
