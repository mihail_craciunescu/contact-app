/**
 * 
 */
package ro.mcr;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.restlet.data.Form;
import org.restlet.resource.ClientResource;

import ro.mcr.data.ContactProfile;
import ro.mcr.rest.IContactResource;
import ro.mcr.rest.IContactsResource;

/**
 * @author mcraciunescu
 *
 */
public class LoadTest {

	public static void main(String[] args) throws Exception {
       checkTestContact();
       int start = checkTotalContacts();
       createTestContacts(start, 10000);
       checkTotalContacts();
	}

	private static int checkTotalContacts() {
		ClientResource clientResource = new ClientResource("http://localhost:8182/rest/contacts/19990009");
		 IContactsResource testResource = clientResource.wrap(IContactsResource.class);

	        // Retrieve the JSON value
	        List<ContactProfile> result = testResource.getContacts();

	        if (result != null) {
	            System.out.println("Total contacts: "+result.size());
	        }
	        return result.size();
	}

	private static void createTestContacts(int start, int n) {
		ClientResource clientResource = new ClientResource("http://localhost:8182/rest/contacts/contact/register");
	        IContactResource testResource = clientResource.wrap(IContactResource.class);
	        final AtomicInteger tSize = new AtomicInteger(0);
	    for (int j=0;j<10;j++){    
	    	final int jj = j;
	    	final Thread t = new Thread(() ->{
	    		
					for(int i=0; i<n; i++){
						try {
							Form entity = new Form();
							int uuid = (start*10000+i+(jj*10000));
							entity.set("mobile", ""+uuid);
							entity.set("name", "user-"+uuid);
							testResource.register(entity );
							Thread.sleep(100);
						} catch (Exception e) {
							System.err.println(e.getMessage());;
						}
					}
					
					tSize.decrementAndGet();
					
		    	}
			);
	    	tSize.incrementAndGet();
	    	t.start();
	    }
	    while(tSize.get() >0 ){
	    	try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	    }
		
	}

	private static void checkTestContact() {
		 ClientResource clientResource = new ClientResource(
		            "http://localhost:8182/rest/contacts/contact/0");
		        IContactResource testResource = clientResource.wrap(IContactResource.class);

		        // Retrieve the JSON value
		        ContactProfile result = testResource.getContact();

		        if (result != null) {
		            System.out.println(result);
		        }
	}
}
